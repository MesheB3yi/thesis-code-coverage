#!/bin/bash

if [ -z "$1" ]
then
    raco make ../wasimilar/wasimilar.rkt && racket ../wasimilar/wasimilar.rkt > src/main.rs
else
    raco make ../wasimilar/wasimilar.rkt && racket ../wasimilar/wasimilar.rkt --seed $1 > src/main.rs
fi

# rustfmt src/main.rs

cargo build --bin harness --release
cargo build --bin harness --target wasm32-wasi --release

rm -rf ./wasmtime-sandbox/*
rm -rf ./wasmer-sandbox/*
rm -rf ./wamr-sandbox/*
rm -rf ./wasmedge-sandbox/*
rm -rf ./wasmi-sandbox/*
rm -rf ./x86-sandbox/*

cp target/wasm32-wasi/release/harness.wasm wasmtime-sandbox
cp target/wasm32-wasi/release/harness.wasm wasmer-sandbox
cp target/wasm32-wasi/release/harness.wasm wamr-sandbox
cp target/wasm32-wasi/release/harness.wasm wasmedge-sandbox
cp target/wasm32-wasi/release/harness.wasm wasmi-sandbox
mv target/release/harness x86-sandbox

cd x86-sandbox
./harness > stdout.log

cd ../wamr-sandbox
iwasm --dir=. harness.wasm > stdout.log

cd ../wasmi-sandbox
wasmi_cli --dir=. harness.wasm > stdout.log

cd ../wasmtime-sandbox
wasmtime --allow-precompiled --dir=. harness.wasm > stdout.log

cd ../wasmer-sandbox
wasmer --dir=. harness.wasm > stdout.log

cd ../wasmedge-sandbox
wasmedge --dir=. harness.wasm > stdout.log

cd ..

./predicate.sh

# tree x86-sandbox
# tree wasi-sandbox

# debug standard outs
# echo "x86: "
# cat x86-sandbox/stdout.log
# echo "wasm: "
# cat wasi-sandbox/stdout.log