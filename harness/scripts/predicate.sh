#!/bin/bash
echo "---- diff " $1 "----"
diff -rq -x harness -x harness.wasm -x clock.log sandboxes/x86-sandbox sandboxes/$1-sandbox
if [[ $? -ne 0 ]]; then
    exit 1
elif [[ -e "sandboxes/x86-sandbox/clock.log" ]]; then
    if [[ "$(wc -l < sandboxes/x86-sandbox/clock.log)" == "$(wc -l < sandboxes/$1-sandbox/clock.log)" ]]; then
        exit 0
    else
        exit 1
    fi
else
    exit 0
fi