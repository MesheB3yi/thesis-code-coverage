// This is a RANDOMLY GENERATED PROGRAM.
// Fuzzer: wasimilar
// Version: wasimilar, xsmith 2.0.6 (de6ace1), in Racket 8.2 (vm-type chez-scheme)
// Options: --seed 1
// Seed: 1
// 

#![allow(warnings)]

use std::io::prelude::*;
use std::fs::OpenOptions;
use rand::{Rng,SeedableRng};
use std::fs;
use rand::seq::SliceRandom;
use rand::rngs::StdRng;

fn main() -> std::io::Result<()>{
let mut rng = StdRng::seed_from_u64(568596);
let mut open_file_ptrs : Vec<fs::File> = Vec::new();
let mut open_file_names : Vec<String> = Vec::new();
let mut closed_files : Vec<String> = Vec::new();
let mut directories : Vec<String> = Vec::new();
directories.push(String::from("./"));

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

if(closed_files.len() > 0){
let index = rng.gen_range(0..closed_files.len());
fs::remove_file(closed_files.remove(index));
}

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

if(closed_files.len() > 0){
let index = rng.gen_range(0..closed_files.len());
open_file_ptrs.push(OpenOptions::new().append(true).read(true).open(closed_files[index].clone()).expect("Failed to open closed file"));
open_file_names.push(closed_files.remove(index));
}

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_1")?;
open_file_ptrs.push(fp);
open_file_names.push("var_1".to_string());

if(closed_files.len() > 0){
let index = rng.gen_range(0..closed_files.len());
open_file_ptrs.push(OpenOptions::new().append(true).read(true).open(closed_files[index].clone()).expect("Failed to open closed file"));
open_file_names.push(closed_files.remove(index));
}

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_2")?;
open_file_ptrs.push(fp);
open_file_names.push("var_2".to_string());

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_3")?;
open_file_ptrs.push(fp);
open_file_names.push("var_3".to_string());

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_4")?;
open_file_ptrs.push(fp);
open_file_names.push("var_4".to_string());

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_5")?;
open_file_ptrs.push(fp);
open_file_names.push("var_5".to_string());

let new_dir = directories.choose(&mut rng).expect("Could not fetch random directory").to_string() + "var_6/";
directories.push(new_dir.clone());
fs::create_dir(new_dir).expect("Failed to create directory");

if(closed_files.len() > 0){
let index = rng.gen_range(0..closed_files.len());
fs::remove_file(closed_files.remove(index));
}

let mut fp = OpenOptions::new().create(true).append(true).read(true).open("var_7")?;
open_file_ptrs.push(fp);
open_file_names.push("var_7".to_string());

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

if(closed_files.len() > 0){
let index = rng.gen_range(0..closed_files.len());
fs::remove_file(closed_files.remove(index));
}

if(open_file_ptrs.len() > 0){
let index = rng.gen_range(0..open_file_ptrs.len());
drop(open_file_ptrs.remove(index));
closed_files.push(open_file_names.remove(index));
}

Ok(())
}


