use std::time::{SystemTime, UNIX_EPOCH};

fn main() {
	let now = SystemTime::now();
	let since_epoch = now.duration_since(UNIX_EPOCH).expect("HUHH??");
	println!("current time: {:?}", since_epoch);
}
